pragma solidity 0.4.24;

import "../node_modules/@aragon/os/contracts/apps/AragonApp.sol";

contract CounterApp is AragonApp {

    // ACL
    bytes32 constant public INCREMENT_ROLE = keccak256("INCREMENT_ROLE");
    bytes32 constant public DECREMENT_ROLE = keccak256("DECREMENT_ROLE");

    // Events
    event Increment(address entity);
    event Decrement(address entity);

    // State
    int public value;

    function initialize() public onlyInit {
        initialized();
    }

    function increment() external auth(INCREMENT_ROLE) {
        value += 1;
        emit Increment(msg.sender);
    }

    function decrement() external auth(DECREMENT_ROLE) {
        value -= 1;
        emit Decrement(msg.sender);
    }

}