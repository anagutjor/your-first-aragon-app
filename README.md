# Your first Aragon app template

> 🕵️ [Find more boilerplates using GitHub](https://github.com/search?q=topic:aragon-boilerplate) | 
> ✨ [Official boilerplates](https://github.com/search?q=topic:aragon-boilerplate+org:aragon)

Template to help you build your first Aragon application following the [tutorial from hack.aragon](https://hack.aragon.org/docs/tutorial.html)


# To run do:
```
npx aragon run
```
or
```
npm run start:app
```
and then 
```
npm run start:aragon:http
```
Then check the Private key for the Address #1 that aragonAPI will automatically generate for you. You need to connect to Metamask on the browser and then:
1. First connect to the local 8545 network
2. Import the account to Metamask using the private key. You can call it something like aragon-test-local.

Now you can use your first Aragon DApp in a local environment.

Note: it takes a while to load on the browser.